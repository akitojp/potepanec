require "rails_helper"

RSpec.describe Spree::Product, type: :model do
  let(:ruby_category) { create(:taxon, name: 'ruby') }
  let(:rails_category) { create(:taxon, name: 'rails') }
  let(:ruby_products) { create_list(:product, 2, taxons: [ruby_category]) }
  let(:rails_products) do
    create_list(:product, 5) do |product|
      product.taxons << rails_category
    end
  end

  describe '#related_products' do
    let(:product) { ruby_products.first }

    it 'assigns related products' do
      expect(product.related_products(4)).to match_array(ruby_products - [product])
    end

    it 'does not assign related products' do
      expect(product.related_products(4)).not_to include rails_products
    end

    it 'is less than 4 items' do
      expect(product.related_products(4).size).to be <= 4
    end
  end

  describe 'scope' do
    describe 'available_now' do
      let!(:two_months_ago_product) { create(:product, available_on: 2.months.ago) }
      let!(:two_days_ago_product) { create(:product, available_on: 2.day.ago) }
      let!(:yesterday_product) { create(:product, available_on: 1.day.ago) }
      let!(:tomorrow_product) { create(:product, available_on: 1.day.after) }

      context 'When products on sale' do
        it 'return some products in no particular order' do
          expect(Spree::Product.available_now).to match_array [
            yesterday_product,
            two_days_ago_product,
            two_months_ago_product,
          ]
        end
      end

      context 'When products before sale' do
        it 'do not return any products' do
          expect(Spree::Product.available_now).not_to include tomorrow_product
        end
      end
    end

    describe 'latest' do
      let!(:two_months_ago_product) { create(:product, available_on: 2.months.ago) }
      let!(:two_days_ago_product) { create(:product, available_on: 2.day.ago) }
      let!(:yesterday_product) { create(:product, available_on: 1.day.ago) }
      let!(:tomorrow_product) { create(:product, available_on: 1.day.after) }

      it 'returns products released within the last month' do
        expect(Spree::Product.latest).to match_array [yesterday_product, two_days_ago_product]
      end

      it 'do not return any products that were released more than one month ago' do
        expect(Spree::Product.latest).not_to include [tomorrow_product, two_months_ago_product]
      end
    end

    describe 'random' do
      let(:number_of_trials) { 1000 }
      let(:product_count)    { 4 } # サンプル数
      let(:probability)      { 1.0 / product_count * 100 } # 同じものが出現する確率
      let(:delta)            { 5.0 } # 誤差

      before do
        create_list(:product, product_count)
        @results = Spree::Product.ids.map { |id| [id, 0] }.to_h
        number_of_trials.times do
          @results[Spree::Product.random(1).first.id] += 1
        end
      end

      shared_examples 'fair level of randomness' do
        it { is_expected.to be_within(delta).of(probability) }
      end

      context 'with the first record' do
        # Recordが出現した確率
        subject { @results[1].to_f / number_of_trials * 100 }

        it_behaves_like 'fair level of randomness'
      end

      context 'with the second record' do
        subject { @results[2].to_f / number_of_trials * 100 }

        it_behaves_like 'fair level of randomness'
      end

      context 'with the third record' do
        subject { @results[3].to_f / number_of_trials * 100 }

        it_behaves_like 'fair level of randomness'
      end

      context 'with the forth record' do
        subject { @results[4].to_f / number_of_trials * 100 }

        it_behaves_like 'fair level of randomness'
      end
    end
  end
end
