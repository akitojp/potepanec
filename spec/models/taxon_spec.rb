require "rails_helper"

RSpec.describe Spree::Taxon, type: :model do
  let!(:taxon) { create(:taxon) }
  let!(:taxon_child) { create(:taxon, parent_id: taxon.id) }
  let!(:products) do
    create_list(:product, 2) do |product|
      product.taxons << taxon
    end
  end
  let!(:child_product) do
    create_list(:product, 2) do |product|
      product.taxons << taxon_child
    end
  end

  describe '#following_all_products' do
    context 'when taxon is leaf' do
      it 'assigns puroducts associated with taxon' do
        expect(taxon_child.following_all_products).to match_array(child_product)
      end
    end
    context 'when taxon is NOT leaf' do
      it 'assigns puroducts associated with taxon' do
        expect(taxon.following_all_products).to match_array(products + child_product)
      end
    end
  end
  describe '#following_all_products_size' do
    context 'when taxon is leaf' do
      it 'returns the number of self puroducts size' do
        expect(taxon_child.following_all_products_size).to eq 2
      end
    end
    context 'when taxon is NOT leaf' do
      it 'returns the number of self and descendants puroducts size' do
        expect(taxon.following_all_products_size).to eq 4
      end
    end
  end
end
