require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe 'GET #index' do
    let!(:two_months_ago_product) { create(:product, available_on: 2.months.ago) }
    let!(:two_days_ago_product) { create(:product, available_on: 2.day.ago) }
    let!(:yesterday_product) { create(:product, available_on: 1.day.ago) }
    let!(:tomorrow_product) { create(:product, available_on: 1.day.after) }

    before do
      get :index
    end

    it 'returns http success' do
      expect(response.status).to eq 200
    end

    it 'renders the index template' do
      expect(response).to render_template :index
    end

    it 'assigns @latest_products' do
      expect(assigns(:latest_products)).to match_array [yesterday_product, two_days_ago_product]
    end
  end
end
