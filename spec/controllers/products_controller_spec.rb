require "rails_helper"

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let(:product) { create(:product) }
    let(:ruby_category) { create(:taxon, name: 'ruby') }
    let(:ruby_products) do
      create_list(:product, 2) do |product|
        product.taxons << ruby_category
      end
    end

    context 'when correct prarams' do
      before { get :show, params: { id: product.id } }

      it 'has a 200 status code' do
        expect(response.status).to eq 200
      end

      it 'assigns @product' do
        expect(assigns(:product)).to eq product
      end

      it 'renders the :show template' do
        expect(response).to render_template :show
      end

      it 'assigns @related_products' do
        product = ruby_products.first
        get :show, params: { id: product.id }
        expect(assigns(:related_products)).to match_array(ruby_products - [product])
      end
    end

    context 'when invalid prarams' do
      before { get :show, params: { id: 99999 } }
      it 'has a 404 status code' do
        expect(response.status).to eq 404
      end
    end
  end
end
