require "rails_helper"
RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show' do
    let(:taxon) { create(:taxon) }
    let(:taxon_child) { create(:taxon, parent_id: taxon.id) }
    let(:taxonomy) { create(:taxonomy) }
    let!(:products) do
      create_list(:product, 2) do |product|
        product.taxons << taxon
      end
    end
    let!(:child_product) do
      create_list(:product, 2) do |product|
        product.taxons << taxon_child
      end
    end

    context 'when correct prarams' do
      before { get :show, params: { id: taxon.id } }
      it 'returns http success' do
        expect(response.status).to eq 200
      end

      it 'renders the show template' do
        expect(response).to render_template :show
      end

      it 'assigns @taxon' do
        expect(assigns(:taxon)).to eq taxon
      end

      it 'assigns @products' do
        expect(assigns(:products)).to match_array(products + child_product)
      end
    end

    context 'when invalid prarams' do
      before { get :show, params: { id: 99999 } }
      it 'has a 404 status code' do
        expect(response.status).to eq 404
      end
    end

    describe 'grid or list view' do
      context 'when params[:display_type] is grid' do
        it 'returns grid page' do
          get :show, params: { id: taxon.id, display_type: 'grid' }
          expect(assigns(:display_type)).to eq 'grid'
        end
      end
      context 'when params[:display_type] is list' do
        it 'returns list page' do
          get :show, params: { id: taxon.id, display_type: 'list' }
          expect(assigns(:display_type)).to eq 'list'
        end
      end
      context 'when params[:display_type] is none' do
        it 'returns grid page' do
          get :show, params: { id: taxon.id }
          expect(assigns(:display_type)).to eq 'grid'
        end
      end
      context 'when params[:display_type] is the others' do
        it 'returns grid page' do
          get :show, params: { id: taxon.id, display_type: 'test' }
          expect(assigns(:display_type)).to eq 'grid'
        end
      end
    end
  end
end
