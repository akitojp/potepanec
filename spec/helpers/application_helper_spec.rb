require "rails_helper"

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    context "The page title is empty." do
      it "Only the base title is displayed" do
        expect(helper.full_title("")).to eq("BIGBAG Store")
      end
    end
    context "The page title is not empty." do
      it "The page title and base title are displayed" do
        expect(helper.full_title("page_title")).to eq('page_title - BIGBAG Store')
      end
    end
  end
end
