require 'rails_helper'

RSpec.feature "Homes", type: :feature do
  feature 'potepan_root_path' do
    given!(:category) { create(:taxon) }
    given!(:product) do
      create(:product, available_on: 1.day.ago) do |product|
        product.taxons << category
        product.images << create(:image)
      end
    end
    background do
      visit potepan_root_path
    end

    feature 'Header' do
      scenario 'The title is correctly displayed' do
        expect(page).to have_title "BIGBAG Store"
      end

      scenario 'After clicking the logo, transit to the home screen' do
        click_on 'Big Bag'
        expect(page).to have_http_status(:success)
        expect(current_path).to eq potepan_root_path
      end

      scenario 'After clicking the navbar home link, transit to the home screen' do
        first('.nav').click_link 'Home'
        expect(page).to have_http_status(:success)
        expect(current_path).to eq potepan_root_path
      end
    end

    feature 'Contents' do
      scenario 'A latest product should be displayed' do
        expect(page).to have_selector 'a', text: product.name
      end
      scenario 'After clicking product link, transit to the product screen' do
        click_link product.name
        expect(page).to have_http_status(:success)
        expect(current_path).to eq potepan_product_path(product.id)
      end
    end
  end
end
