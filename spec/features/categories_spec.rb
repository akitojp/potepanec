require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  feature 'potepan_category_path(category.id)' do
    given!(:category) { create(:taxon) }
    given!(:product) do
      create(:product) do |product|
        product.taxons << category
        product.images << create(:image)
      end
    end

    background do
      visit potepan_category_path(category.id)
    end

    feature 'Header' do
      scenario 'The page title is correctly displayed' do
        expect(page).to have_selector 'h2', text: category.name
      end

      scenario 'After clicking the logo, transit to the home screen' do
        click_on 'Big Bag'
        expect(page).to have_css '.bannercontainer'
      end

      scenario 'After clicking the navbar home link, transit to the home screen' do
        first('.nav').click_link 'Home'
        expect(page).to have_css '.bannercontainer'
      end

      scenario 'After clicking the breadcrumb home link, transit to the home screen' do
        first('.breadcrumb').click_link 'Home'
        expect(page).to have_css '.bannercontainer'
      end
    end

    feature 'Display type' do
      scenario 'The display type is grid' do
        expect(page).to have_css '.productBox'
        expect(page).to have_no_css '.productListSingle'
      end

      scenario 'After clicking glid button, The display type will be grid' do
        click_link 'Grid'
        expect(page).to have_css '.productBox'
        expect(page).to have_no_css '.productListSingle'
      end

      scenario 'After clicking list button, The display type will be list' do
        click_link 'List'
        expect(page).to have_css '.productListSingle'
        expect(page).to have_no_css '.productBox'
      end
    end

    feature 'Sidebar' do
      scenario 'After clicking the category link, transit to the category screen' do
        find('li', text: category.name).click
        expect(page).to have_selector 'h2', text: category.name
      end
    end

    feature 'Contents' do
      feature 'when display type is grid' do
        background do
          click_link 'Grid'
        end
        scenario 'Product belonging to the category should be displayed' do
          expect(page).to have_selector 'h5', text: product.name
        end
        scenario 'After clicking product link, transit to the product screen' do
          click_link product.name
          expect(page).to have_selector '.page-title h2', text: product.name
        end
      end
      feature 'when display type is list' do
        background do
          click_link 'List'
        end
        scenario 'Product belonging to the category should be displayed' do
          expect(page).to have_selector '.media-heading', text: product.name
        end
        scenario 'After clicking product link, transit to the product screen' do
          click_link product.name
          expect(page).to have_selector '.page-title h2', text: product.name
        end
      end
    end
  end
end
