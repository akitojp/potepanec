require 'rails_helper'

RSpec.feature 'Products', type: :feature do
  feature 'potepan_product_path(product.id)' do
    given!(:category) { create(:taxon) }
    given!(:product) do
      create(:product) do |product|
        product.taxons << category
        product.images << create(:image)
      end
    end
    given!(:related_product) do
      create(:product) do |product|
        product.taxons << category
        product.images << create(:image)
      end
    end

    background do
      visit potepan_product_path(product.id)
    end

    feature 'Header' do
      scenario 'After clicking the logo, transit to the home screen' do
        click_on 'Big Bag'
        expect(page).to have_css '.bannercontainer'
      end

      scenario 'After clicking the navbar home link, transit to the home screen' do
        first('.nav').click_link 'Home'
        expect(page).to have_css '.bannercontainer'
      end

      scenario 'After clicking the breadcrumb home link, transit to the home screen' do
        first('.breadcrumb').click_link 'Home'
        expect(page).to have_css '.bannercontainer'
      end
    end

    feature 'Contents' do
      scenario 'The related_product is correctly displayed' do
        expect(page).to have_link related_product.name
      end

      scenario 'After clicking the related_product link, transit to the related product page' do
        click_on related_product.name
        expect(page).to have_selector '.page-title h2', text: related_product.name
      end
    end
  end
end
