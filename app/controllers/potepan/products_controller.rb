class Potepan::ProductsController < ApplicationController
  before_action :set_product, only: [:show]

  MAX_RELATED_PRODUCTS = 4

  def show
    @related_products = @product.related_products(MAX_RELATED_PRODUCTS)
  end

  private

  def set_product
    @product = Spree::Product.find(params[:id])
  end
end
