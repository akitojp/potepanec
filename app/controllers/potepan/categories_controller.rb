class Potepan::CategoriesController < ApplicationController
  before_action :set_category, only: [:show]

  def show
    @taxonomies = Spree::Taxonomy.includes(taxons: :children)
    @products = @taxon.following_all_products
    @display_type = params[:display_type] == 'list' ? 'list' : 'grid'
  end

  private

  def set_category
    @taxon = Spree::Taxon.find(params[:id])
  end
end
