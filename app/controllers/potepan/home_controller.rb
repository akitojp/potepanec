class Potepan::HomeController < ApplicationController
  MAX_LATEST_PRODUCTS = 4

  def index
    @latest_products = Spree::Product.with_price_and_image.latest.random(MAX_LATEST_PRODUCTS)
  end
end
