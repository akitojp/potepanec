Spree::Taxon.class_eval do
  def following_all_products
    if leaf?
      active_products.with_price_and_image
    else
      self_and_descendants.map { |taxon| taxon.active_products.with_price_and_image }.flatten.uniq
    end
  end

  def following_all_products_size
    if leaf?
      active_products.size
    else
      self_and_descendants.map { |taxon| taxon.active_products }.flatten.uniq.size
    end
  end
end
