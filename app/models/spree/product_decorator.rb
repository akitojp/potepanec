Spree::Product.class_eval do
  scope :with_price_and_image, -> { includes(master: [:default_price, :images]) }
  scope :available_now, -> { where('available_on <= ?', Time.zone.now) }
  scope :latest, -> { where(available_on: (1.month.ago)..(Time.zone.now)) }
  scope :random, -> (num) { where(id: pluck(:id).sample(num)) }

  def related_products(quantity)
    Spree::Product.
      with_price_and_image.
      joins(:taxons).
      where(spree_taxons: { id: taxons.ids }).
      where.not(id: id).
      limit(quantity).
      distinct
  end
end
